from django.db import models
from django.utils import timezone

# Create your models here.


class Tweet(models.Model):
    text = models.TextField()
    data_pub = models.DateTimeField(default=timezone.now())
