from django.shortcuts import render, redirect
from .forms import FormTweet
from .models import Tweet

# Create your views here.


def tweet_list(request):
    tweets = Tweet.objects.all().order_by('-data_pub')
    form = FormTweet(request.POST or None)

    if request.method == "POST" and form.is_valid():
        new_tweet = Tweet()
        new_tweet.text = form.cleaned_data['text']
        new_tweet.save()
        return redirect('tweet_list')

    else:
        form = FormTweet()

    return render(request, 'tweet/tweet_list.html', {'tweets': tweets, 'form': form})